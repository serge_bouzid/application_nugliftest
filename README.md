Application_NuglifTest
========================



Description
-----------

Application Nuglif (La Presse +) test

---



Requirement
-----------

- Script language: PHP: version 5.6 || 7

---



Installation
------------

1. Command: Move in project parent directory path
    
    ```sh
    cd "<project_parent_dir_path>"
    ```

2. Command: Installation
    
    ```sh
    git clone https://bitbucket.org/serge_bouzid/application_nugliftest.git
    ```

3. Command: Install external libraries
    
    ```sh
    php composer.phar dump-autoload
    ```

---



Usage
---------------------

- Run test

    ```sh
    php bin/app
    ```
    
---


