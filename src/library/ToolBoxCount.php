<?php
/**
 * @copyright Copyright (c) 2021 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace application\nuglif_test\library;

class ToolBoxCount
{
    /**
     * Get specified index array of string labels,
     * from specified number,
     * and specified label configuration.
     *
     * Array of label configuration format:
     * Key: 'string label' => value: integer divider
     *
     * @param integer $number
     * @param array $tabConfigLabel
     * @return string[]
     */
    protected static function getTabNumLabel($number, array $tabConfigLabel)
    {
        $result = array();

        // Run each label config, if required
        if (is_int($number)) {
            foreach ($tabConfigLabel as $label => $divider) {
                // Register label, if required
                if (
                    is_string($label) &&
                    is_int($divider) &&
                    (($number % $divider) == 0)
                ) {
                    $result[] = $label;
                }
            }
        }

        return $result;
    }



    /**
     * Get string render,
     * from specified number,
     * and specified index array of labels.
     *
     * @param integer $number
     * @param string[] $tabLabel
     * @return string
     */
    protected static function getNumRender($number, array $tabLabel)
    {
        $result = strval($number);

        // Get labels render, if required
        if (count($tabLabel) > 0) {
            $renderLabel = '';
            foreach ($tabLabel as $label) {
                $renderLabel .=
                    ((trim($renderLabel) != '') ? ', ' : '') .
                    $label;
            }

            $result = sprintf('%1$s: %2$s', $result, $renderLabel);
        }

        return $result;

    }



    /**
     * Get string render,
     * from specified count,
     * and specified label configuration.
     *
     * Array of label configuration format:
     * @see getTabNumLabel() array of label configuration format.
     *
     * @param integer $count
     * @param array $tabLabel
     * @return string
     */
    public static function getCountRender($count, array $tabLabel)
    {
        $result = '';

        // Run each number, if required
        if (
            is_int($count) &&
            ($count >= 1)
        ) {
            for ($cpt = 1; $cpt <= $count; $cpt++) {
                // Build count render
                $result .=
                    ((trim($result) != '') ? PHP_EOL : '') .
                    static::getNumRender($cpt, static::getTabNumLabel($cpt, $tabLabel));
            }
        }

        return $result;
    }
}